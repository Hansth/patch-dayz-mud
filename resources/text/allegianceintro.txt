\n\r\n\r
^W+^k---------------------------------------------------------------------------^W+^k
| ^WIts time to choose your ^bAllegiance^W. Your ^bAllegiance^W is similar to a       ^k|
| ^Wfaction. The ^wGelf^W and ^YSkexie^W have been at war for years. When you see a   ^k|
| ^Wmember of the opposite ^ballegiance^W, if you are within 15 levels it is open ^k|
| ^WPK, regardless of their PK flag. If you travel to a city loyal to the     ^k|
| ^Wopposite ^ballegiance^W as you, the guards will attack on site.               ^k|
^W+^k---------------------------------------------------------------------------^W+^k
| ^WAllegiance     ^k| ^WDescription                                              ^k|
^W+^k---------------------------------------------------------------------------^W+^k
| ^wGelf           ^k| ^wGelfing ^WAllegiance description goes here.                ^k|
|                ^k|                                                          ^k|
|                ^k|                                                          ^k|
|                ^k|                                                          ^k|
^W+^k---------------------------------------------------------------------------^W+^k
| ^YSkexie         ^k| ^YSkexie ^WAllegiance description goes here.                 ^k|
|                ^k|                                                          ^k|
|                ^k|                                                          ^k|
|                ^k|                                                          ^k|
^W+^k---------------------------------------------------------------------------^W+



