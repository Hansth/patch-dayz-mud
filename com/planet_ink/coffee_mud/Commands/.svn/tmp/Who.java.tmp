package com.planet_ink.coffee_mud.Commands;
import com.planet_ink.coffee_mud.core.interfaces.*;
import com.planet_ink.coffee_mud.core.*;
import com.planet_ink.coffee_mud.core.collections.*;
import com.planet_ink.coffee_mud.Abilities.interfaces.*;
import com.planet_ink.coffee_mud.Areas.interfaces.*;
import com.planet_ink.coffee_mud.Behaviors.interfaces.*;
import com.planet_ink.coffee_mud.CharClasses.interfaces.*;
import com.planet_ink.coffee_mud.Commands.interfaces.*;
import com.planet_ink.coffee_mud.Common.interfaces.*;
import com.planet_ink.coffee_mud.Exits.interfaces.*;
import com.planet_ink.coffee_mud.Items.interfaces.*;
import com.planet_ink.coffee_mud.Libraries.interfaces.*;
import com.planet_ink.coffee_mud.Locales.interfaces.*;
import com.planet_ink.coffee_mud.MOBS.interfaces.*;
import com.planet_ink.coffee_mud.Races.interfaces.*;

import java.util.*;

/*
   Copyright 2004-2015 Bo Zimmerman

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

	++Modified for Patch Dayz Mud by Hansth aka Darren Steinke 2015++
*/
@SuppressWarnings({"unchecked","rawtypes"})
public class Who extends StdCommand
{
	public Who(){}

	private final String[] access=I(new String[]{"WHO","WH"});
	@Override public String[] getAccessWords(){return access;}

        int mor = 0;
        int imm = 0;

	private final static Class[][] filterParameters=new Class[][]{{Boolean.class,Filterer.class}};

	public int[] getShortColWidths(MOB seer)
	{
		return new int[]{
			CMLib.lister().fixColWidth(12,seer.session()),
			CMLib.lister().fixColWidth(12,seer.session()),
			CMLib.lister().fixColWidth(7,seer.session()),
			CMLib.lister().fixColWidth(40,seer.session())
		};
	}

	public String getHead(int[] colWidths)
	{
		final StringBuilder head=new StringBuilder("");
                head.append("^RImm^rort^Rals^N\n\r");
		head.append("^W^_[");
			head.append(CMStrings.padRight(L("^W^_Department"),colWidths[0])+" ");
			head.append(CMStrings.padRight(L("^W^_Class"),colWidths[1])+" ");
			head.append(CMStrings.padRight(L("^W^_     "),colWidths[2]));
		head.append("^W^_] ^W^_Name^N\n\r");
		return head.toString();
	}

	public String getHead2(int[] colWidths)
	{
		final StringBuilder head2=new StringBuilder("");
                head2.append("^GMo^grta^Gls^N\n\r");
                head2.append("^W^_[");
                        head2.append(CMStrings.padRight(L("^W^_Race"),colWidths[0])+" ");
                        head2.append(CMStrings.padRight(L("^W^_Class"),colWidths[1])+" ");
                        head2.append(CMStrings.padRight(L("^W^_Level"),colWidths[2]));
                head2.append("^W^_] ^W^_Name^N\n\r");
                return head2.toString();
	}

	public StringBuffer showWhoShort(MOB who, int[] colWidths)
	{
		final StringBuffer msg=new StringBuffer("");
		final StringBuffer msg2=new StringBuffer("");
	if (CMSecurity.isAllowedAnywhere(who,CMSecurity.SecFlag.WIZINV))
		{
		msg.append("^k[");
		msg.append("^b"+CMStrings.padRight(who.playerStats().getDepartment(),colWidths[0])+" ");

		String levelStr=who.charStats().displayClassLevel(who,true).trim();
		final int x=levelStr.lastIndexOf(' ');
			if(x>=0)
				levelStr=levelStr.substring(x).trim();
		String iname2=null;
		String iname3="Hansth";
		iname2=(who.Name());
		if (iname3.equals(iname2)){ 
		msg.append("^c"+CMStrings.padRight("Implementor",colWidths[1])+" ");}
		else {
		msg.append("^c"+CMStrings.padRight(who.charStats().displayClassName(),colWidths[1])+" ");}
		msg.append("^c"+CMStrings.padRight("   ",colWidths[2])+"");
		String iname=null;
		if(CMath.bset(who.phyStats().disposition(),PhyStats.IS_CLOAKED))
			iname="("+(who.Name().equals(who.name())?who.titledName():who.name())+")";
		else
			iname=(who.Name().equals(who.name())?who.titledName():who.name());
		if((who.session()!=null)&&(who.session().isAfk()))
			iname=iname+(" (AFK: "+CMLib.time().date2BestShortEllapsedTime(who.session().getIdleMillis())+")");
		msg.append("^k] ^w"+CMStrings.padRight(iname,colWidths[3]));
		msg.append("\n\r");
		return msg;
		}
	else
		{
                msg2.append("^k[");
                msg2.append("^b"+CMStrings.padRight(who.charStats().raceName(),colWidths[0])+" ");

                String levelStr=who.charStats().displayClassLevel(who,true).trim();
                final int x=levelStr.lastIndexOf(' ');
                        if(x>=0)
                                levelStr=levelStr.substring(x).trim();
                msg2.append("^c"+CMStrings.padRight(who.charStats().displayClassName(),colWidths[1])+" ");
                msg2.append("^C"+CMStrings.padRight(levelStr,colWidths[2]));

                String mname=null;
                if(CMath.bset(who.phyStats().disposition(),PhyStats.IS_CLOAKED))
                        mname="("+(who.Name().equals(who.name())?who.titledName():who.name())+")";
                else
                        mname=(who.Name().equals(who.name())?who.titledName():who.name());
                if((who.session()!=null)&&(who.session().isAfk()))
                        mname=mname+(" (AFK: "+CMLib.time().date2BestShortEllapsedTime(who.session().getIdleMillis())+")");
                msg2.append("^k] ^w"+CMStrings.padRight(mname,colWidths[3]));
                msg2.append("\n\r");
                return msg2;
		}
	}

	@Override
	public boolean execute(MOB mob, List<String> commands, int metaFlags)
		throws java.io.IOException
	{
		int whototal = 0;
                final int[] colWidths=getShortColWidths(mob);
		String mobName=CMParms.combine(commands,1);
		if((mobName!=null)
		&&(mob!=null)
		&&(mobName.startsWith("@")))
		{
			if((!(CMLib.intermud().i3online()))
			&&(!CMLib.intermud().imc2online()))
				mob.tell(L("Intermud is unavailable."));
			else
				CMLib.intermud().i3who(mob,mobName.substring(1));
			return false;
		}
		Set<String> friends=null;
		if((mobName!=null)
		&&(mob!=null)
		&&(mobName.equalsIgnoreCase("friends"))
		&&(mob.playerStats()!=null))
		{
			friends=mob.playerStats().getFriends();
			mobName=null;
		}

		if((mobName!=null)
		&&(mob!=null)
		&&(mobName.equalsIgnoreCase("pk")
		||mobName.equalsIgnoreCase("pkill")
		||mobName.equalsIgnoreCase("playerkill")))
		{
			friends=new HashSet<String>();

			for(final Session S : CMLib.sessions().allIterable())
			{
				final MOB mob2=S.mob();
				if((mob2!=null)&&(mob2.isAttributeSet(MOB.Attrib.PLAYERKILL)))
					friends.add(mob2.Name());
			}
		}

		StringBuffer msg = new StringBuffer("");
		StringBuffer msg2 = new StringBuffer("");

                for(final Session S : CMLib.sessions().localOnlineIterable())
                {
                        MOB mob2=S.mob();
                        if((mob2!=null)&&(mob2.soulMate()!=null))
                                mob2=mob2.soulMate();

                        if((mob2!=null)
                        &&((((mob2.phyStats().disposition()&PhyStats.IS_CLOAKED)==0)||((CMSecurity.isAllowedAnywhere(mob,CMSecurity.SecFlag.CLOAK))))
                        &&((friends==null)||(friends.contains(mob2.Name())||(friends.contains("All"))))
			&&(CMSecurity.isAllowedAnywhere(mob2,CMSecurity.SecFlag.WIZINV))))
					{
						msg.append(showWhoShort(mob2,colWidths));
						imm += 1;
					}
                        if((mob2!=null)
                        &&((((mob2.phyStats().disposition()&PhyStats.IS_CLOAKED)==0)||((CMSecurity.isAllowedAnywhere(mob,CMSecurity.SecFlag.CLOAK))))
                        &&((friends==null)||(friends.contains(mob2.Name())||(friends.contains("All"))))
                        &&(!CMSecurity.isAllowedAnywhere(mob2,CMSecurity.SecFlag.WIZINV)))) 
                                       {
                                                msg2.append(showWhoShort(mob2,colWidths));
                                                mor += 1;
                                        }
                }

		StringBuffer head = new StringBuffer("");
			head.append(getHead(colWidths));
		StringBuffer head2 = new StringBuffer("");
			head2.append(getHead2(colWidths));
			whototal = imm + mor;
			mob.tell(head.toString()+msg.toString()
			+"\n\n\r\n\r\n"+head2.toString()+msg2.toString()+"\n\r\n\r");
                        mob.tell(L("^w+^k----------------------^w+"));
                        mob.tell(L("^k| ^RImm^rort^Rals ^ROn^rli^Rne^W: " + imm + "^k  |"));
                        mob.tell(L("^k| ^GMo^grta^Gls ^GOn^gli^Gne  ^W: " + mor + "^k  |"));
                        mob.tell(L("^k| ^BTo^bt^Bal ^BPl^baye^Brs   ^W: " + whototal + "^k  |"));
                        mob.tell(L("^w+^k----------------------^w+^N"));
			imm = 0;
			mor = 0;
		return false;
	}

	@Override public boolean canBeOrdered(){return true;}
}

