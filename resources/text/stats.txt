\n\r
^W+^k---------------------------------------------------------------------------^W+^k
| ^WYour stats below reflect your physical and mental gifts.  They can be     ^k|
| ^Wboosted through the use of Training points, which are granted at first    ^k|
| ^Wlevel, and are gained at a rate of one per level.  These training points  ^k|
| ^Ware also needed to gain new spells and abilities which your class         ^k|
| ^Wqualifies for, but does not get at first level.                           ^k|
^W+^k---------------------------------------------------------------------------^W+^k
| ^WStat           ^k| ^WDescription                                              ^k|
^W+^k---------------------------------------------------------------------------^W+^k
| ^wStrength       ^k| ^WPhysical strength and fighting prowess is reflected in   ^k|
|                | ^Wstrength. ^cWarriors^W and ^cPaladins^W love their muscles.      ^k|
^W+^k---------------------------------------------------------------------------^W+^k
| ^wDexterity      ^k| ^WAgility and the ability to dodge blows is reflected in   ^k|
|                | ^WDexterity. ^cRogues^W, ^cMonks^W, ^cShamans ^Wand ^cHunters^W need all of^k|
|                | ^Wthis they can get.                                       ^k|
^W+^k---------------------------------------------------------------------------^W+^k
| ^wConstitution   ^k| ^WHealth, stamina, and the ability to live through harder  ^k|
|                | ^Wfights is reflected in Constitution. ^cWarriors ^Wand ^cHunters^k|
|                | ^Wadore health.                                            ^k|
^W+^k---------------------------------------------------------------------------^W+^k
| ^wIntelligence   ^k| ^WMemory and concentration, and the ability to gain greater^k|
|                | ^Wproficiency in skills is reflected in Intelligence. ^cMages^k|
|                | ^cPriests^W, ^cDruids^W, ^cMonks ^Wand ^cWarlocks ^Wrequire intellect.   ^k|
^W+^k---------------------------------------------------------------------------^W+^k
| ^wWisdom         ^k| ^WIntuition, wit, and the ability to gain more practice    ^k|
|                | ^Wpoints per level is reflected in Wisdom. ^cPriests^W, ^cMages^W, ^k|
|                | ^cDruids^W, ^cPaladins ^Wand ^cShamans ^Wuse wisdom best.            ^k|
^W+^k---------------------------------------------------------------------------^W+^k
| ^wCharisma       ^k| ^WGrace, charm, and the ability to get better prices from  ^k|
|                | ^Wmerchants is reflected in Charisma. ^cBards ^Wand ^cRogues ^Wlive^k|
|                | ^Woff their charm.                                         ^k|
^W+^k---------------------------------------------------------------------------^W+

