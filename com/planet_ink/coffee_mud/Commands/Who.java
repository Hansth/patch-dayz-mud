package com.planet_ink.coffee_mud.Commands;
import com.planet_ink.coffee_mud.core.interfaces.*;
import com.planet_ink.coffee_mud.core.*;
import com.planet_ink.coffee_mud.core.collections.*;
import com.planet_ink.coffee_mud.Abilities.interfaces.*;
import com.planet_ink.coffee_mud.Areas.interfaces.*;
import com.planet_ink.coffee_mud.Behaviors.interfaces.*;
import com.planet_ink.coffee_mud.CharClasses.interfaces.*;
import com.planet_ink.coffee_mud.Commands.interfaces.*;
import com.planet_ink.coffee_mud.Common.interfaces.*;
import com.planet_ink.coffee_mud.Exits.interfaces.*;
import com.planet_ink.coffee_mud.Items.interfaces.*;
import com.planet_ink.coffee_mud.Libraries.interfaces.*;
import com.planet_ink.coffee_mud.Locales.interfaces.*;
import com.planet_ink.coffee_mud.MOBS.interfaces.*;
import com.planet_ink.coffee_mud.Races.interfaces.*;

import java.util.*;

/*
   Copyright 2004-2015 Bo Zimmerman

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

	++Modified for Patch Dayz Mud by Hansth aka Darren Steinke 2015++
*/
@SuppressWarnings({"unchecked","rawtypes"})
public class Who extends StdCommand
{
	public Who(){}

	private final String[] access=I(new String[]{"WHO","WH"});
	@Override public String[] getAccessWords(){return access;}

        int mor = 0;
        int imm = 0;
	public int cnum = 0;

	private final static Class[][] filterParameters=new Class[][]{{Boolean.class,Filterer.class}};

	public int[] getShortColWidths(MOB seer)
	{
		return new int[]{
			CMLib.lister().fixColWidth(12,seer.session()),
			CMLib.lister().fixColWidth(12,seer.session()),
			CMLib.lister().fixColWidth(7,seer.session()),
			CMLib.lister().fixColWidth(40,seer.session()),
			CMLib.lister().fixColWidth(20,seer.session())
		};
	}

	public String getHead(int[] colWidths)
	{
		final StringBuilder head=new StringBuilder("");
                head.append("^RImm^rort^Rals^N\n\r");
		head.append("^W^_[");
			head.append(CMStrings.padRight(L("^W^_Department"),colWidths[0])+" ");
			head.append(CMStrings.padRight(L("^W^_Class"),colWidths[1])+" ");
			head.append(CMStrings.padRight(L("^W^_     "),colWidths[2]));
		head.append("^W^_] ^W^_Name^N\n\r");
		return head.toString();
	}

	public String getHead2(int[] colWidths)
	{
		final StringBuilder head2=new StringBuilder("");
                head2.append("^GMo^grta^Gls^N\n\r");
                head2.append("^W^_[");
                        head2.append(CMStrings.padRight(L("^W^_Race"),colWidths[0])+" ");
                        head2.append(CMStrings.padRight(L("^W^_Class"),colWidths[1])+" ");
                        head2.append(CMStrings.padRight(L("^W^_Level"),colWidths[2]));
                head2.append("^W^_] ^W^_Name^N\n\r");
                return head2.toString();
	}

	// This is for CLANWHO -Hansth 2015
	public String getHead3(int[] colWidths)
	{
		final StringBuilder head3=new StringBuilder("");
                head3.append("^kMe^Ymbe^krs On^Yli^kne^N:\n\r\n\r");
                head3.append("^W^_[");
                        head3.append(CMStrings.padRight(L("^W^_Rank"),colWidths[4])+" ");
                        head3.append(CMStrings.padRight(L("^W^_Location"),colWidths[4])+" ");
                        head3.append(CMStrings.padRight(L("^W^_Level"),colWidths[0]));
                head3.append("^W^_] ^W^_Name^N\n\r");
                return head3.toString();
	}

	public StringBuffer showWhoClan(MOB who, int[] colWidths)
	{
		final StringBuffer cmsg=new StringBuffer("");
                if(who.clans().iterator().hasNext())
                {
                        for(final Iterator<Pair<Clan,Integer>> c = who.clans().iterator();c.hasNext();)
                        {

                        final Pair<Clan,Integer> p=c.next();
                        final Clan C=p.first;
			String role=C.getRoleName(p.second.intValue(),true,false);
                	String levelStr=who.charStats().displayClassLevel(who,true).trim();
                	final int x=levelStr.lastIndexOf(' ');
                        if(x>=0)
                                levelStr=levelStr.substring(x).trim();
                        final StringBuilder showClanRank=new StringBuilder("");
			final StringBuilder showCharLoc=new StringBuilder("");
			final StringBuilder showCharLevel=new StringBuilder("");
			final StringBuilder showCharName=new StringBuilder("");
			cmsg.append("^W[^b");
			showClanRank.append(role);
			cmsg.append(CMStrings.padRight(showClanRank.toString(),colWidths[4]));
			showCharLoc.append("^C "+CMLib.map().getExtendedRoomID(who.location()));
			cmsg.append(CMStrings.padRight(showCharLoc.toString(),colWidths[4]));
			showCharLevel.append("^c "+levelStr);
			cmsg.append(CMStrings.padRight(showCharLevel.toString(), colWidths[0])+"  ");
			cmsg.append("^W] ^w");
			showCharName.append(who.Name());
			cmsg.append(CMStrings.padRight(showCharName.toString(), colWidths[0]));
			cmsg.append("^N\n\r");
			cnum += 1;
                        }

                }
			return cmsg;

	}

	public StringBuffer showWhoShort(MOB who, int[] colWidths)
	{
		final StringBuffer msg=new StringBuffer("");
		final StringBuffer msg2=new StringBuffer("");
	if (CMSecurity.isAllowedAnywhere(who,CMSecurity.SecFlag.WIZINV))
		{
		msg.append("^k[");
		msg.append("^b"+CMStrings.padRight(who.playerStats().getDepartment(),colWidths[0])+" ");

		String levelStr=who.charStats().displayClassLevel(who,true).trim();
		final int x=levelStr.lastIndexOf(' ');
			if(x>=0)
				levelStr=levelStr.substring(x).trim();
		String iname2=null;
		String iname3="Hansth";
		String iname4="Azrillee";
		String iname5="Scythe";
		iname2=(who.Name());
		if ((iname3.equals(iname2))||(iname4.equals(iname2))||(iname5.equals(iname2)))
			{ 
				msg.append("^c"+CMStrings.padRight("Implementor",colWidths[1])+" ");
			}
		else
		msg.append("^c"+CMStrings.padRight("Immortal",colWidths[1])+" ");

		msg.append("^c"+CMStrings.padRight("   ",colWidths[2])+"");
		msg.append("^k] ^N");

                if(who.clans().iterator().hasNext())
                {
                        for(final Iterator<Pair<Clan,Integer>> c = who.clans().iterator();c.hasNext();)
                        {
                                final Pair<Clan,Integer> p=c.next();
                                final Clan C=p.first;
                        final StringBuilder showClan=new StringBuilder("");
                        showClan.append("^w["+C.getClanAlias()+"^w] ^N");
                        msg.append(L(showClan.toString()));
                        }

                }
		String iname=getWhoName(who);
		msg.append(""+CMStrings.padRight(iname,colWidths[3]));
//		msg.append("\n\r");
		return msg;
		}
	else
		{
                msg2.append("^k[");
                msg2.append("^b"+CMStrings.padRight(who.charStats().raceName(),colWidths[0])+" ");

                String levelStr=who.charStats().displayClassLevel(who,true).trim();
                final int x=levelStr.lastIndexOf(' ');
                        if(x>=0)
                                levelStr=levelStr.substring(x).trim();
                msg2.append("^c"+CMStrings.padRight(who.charStats().displayClassName(),colWidths[1])+" ");
                msg2.append("^C"+CMStrings.padRight(levelStr,colWidths[2]));
		msg2.append("^k] ^w");
                if(who.clans().iterator().hasNext())
                {
                        for(final Iterator<Pair<Clan,Integer>> c = who.clans().iterator();c.hasNext();)
                        {
                                final Pair<Clan,Integer> p=c.next();
                                final Clan C=p.first;
                        final StringBuilder showClan2=new StringBuilder("");
                        showClan2.append("^w["+C.getClanAlias()+"^w] ^N");
                        msg2.append(L(showClan2.toString()));
                        }

                }
		String mname=getWhoName(who);
                msg2.append(CMStrings.padRight(mname,colWidths[3]));
                msg2.append("\n\r");
                return msg2;
		}
	}

	public String getWhoName(MOB seenM)
	{
		String name=null;  
		if(CMath.bset(seenM.phyStats().disposition(),PhyStats.IS_CLOAKED))  
			name="("+(seenM.Name().equals(seenM.name())?seenM.titledName():seenM.name())+")";  
		else  
			name=(seenM.Name().equals(seenM.name())?seenM.titledName():seenM.name());  
		if((seenM.session()!=null)&&(seenM.session().isAfk()))  
			name=name+(" (AFK: "+CMLib.time().date2BestShortEllapsedTime(seenM.session().getIdleMillis())+")");  
		return name;  
	}

	public boolean checkWho(MOB seerM, MOB seenM, Set<String> friends, Filterer<MOB> mobFilter)  
	{  
		if((seenM!=null)  
		&&((((seenM.phyStats().disposition()&PhyStats.IS_CLOAKED)==0)  
			||((CMSecurity.isAllowedAnywhere(seerM,CMSecurity.SecFlag.CLOAK)||CMSecurity.isAllowedAnywhere(seerM,CMSecurity.SecFlag.WIZINV))&&(seerM.phyStats().level()>=seenM.phyStats().level()))))  
		&&((friends==null)||(friends.contains(seenM.Name())||(friends.contains("All"))))  
		&&((mobFilter==null)||(mobFilter.passesFilter(seenM)))  
		&&(seenM.phyStats().level()>0))  
			return true;  
		return false;  
	}  

	@Override
	public boolean execute(MOB mob, List<String> commands, int metaFlags)
		throws java.io.IOException
	{
		int whototal = 0;
                final int[] colWidths=getShortColWidths(mob);
		String mobName=CMParms.combine(commands,1);
		if((mobName!=null)
		&&(mob!=null)
		&&(mobName.startsWith("@")))
		{
			if((!(CMLib.intermud().i3online()))
			&&(!CMLib.intermud().imc2online()))
				mob.tell(L("Intermud is unavailable."));
			else
				CMLib.intermud().i3who(mob,mobName.substring(1));
			return false;
		}
		Set<String> friends=null;
		if((mobName!=null)
		&&(mob!=null)
		&&(mobName.equalsIgnoreCase("friends"))
		&&(mob.playerStats()!=null))
		{
			friends=mob.playerStats().getFriends();
			mobName=null;
		}

		if((mobName!=null)
		&&(mob!=null)
		&&(mobName.equalsIgnoreCase("pk")
		||mobName.equalsIgnoreCase("pkill")
		||mobName.equalsIgnoreCase("playerkill")))
		{
			friends=new HashSet<String>();

			for(final Session S : CMLib.sessions().allIterable())
			{
				final MOB mob2=S.mob();
				if((mob2!=null)&&(mob2.isAttributeSet(MOB.Attrib.PLAYERKILL)))
					friends.add(mob2.Name());
			}
		}

		StringBuffer msg = new StringBuffer("");
		StringBuffer msg2 = new StringBuffer("");

                for(final Session S : CMLib.sessions().localOnlineIterable())
                {
                        MOB mob2=S.mob();
                        if((mob2!=null)&&(mob2.soulMate()!=null))
                                mob2=mob2.soulMate();

			if(checkWho(mob,mob2,friends,null))
					{
						msg.append(showWhoShort(mob2,colWidths));
						imm += 1;
					}
			else
                                       {
                                                msg2.append(showWhoShort(mob2,colWidths));
                                                mor += 1;
                                       }
                }

		StringBuffer head = new StringBuffer("");
			head.append(getHead(colWidths));
		StringBuffer head2 = new StringBuffer("");
			head2.append(getHead2(colWidths));
			whototal = imm + mor;
			mob.tell(head.toString()+msg.toString()
			+"\n\r\n\r"+head2.toString()+msg2.toString()+"\n\r\n\r");
                        mob.tell(L("^w+^k----------------------^w+"));
                        mob.tell(L("^k| ^RImm^rort^Rals ^ROn^rli^Rne^W: " + imm + "^k  |"));
                        mob.tell(L("^k| ^GMo^grta^Gls ^GOn^gli^Gne  ^W: " + mor + "^k  |"));
                        mob.tell(L("^k| ^BTo^bt^Bal ^BPl^baye^Brs   ^W: " + whototal + "^k  |"));
                        mob.tell(L("^w+^k----------------------^w+^N"));
			imm = 0;
			mor = 0;
		return false;
	}

	@Override public boolean canBeOrdered(){return true;}
}

