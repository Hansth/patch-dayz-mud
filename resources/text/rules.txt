^W+^k--------------------------------------------------------------------------^W+
^k|                                  ^PR^pule^Ps               ^k                    |
^k|                        ^pP^Patch Dayz Development Por^pt ^k                      |
^W+^k--------------------------------------------------------------------------^W+
^k|                                                                          |
^k| ^Y* ^NTreat newbies with respect. Do not harass them or camp them.        ^k   |
^k|                                                                          |
^k| ^Y* ^NRoleplaying is encouraged, but not enforced.                       ^k    |
^k|                                                                          |
^k| ^Y* ^NExploiting a bug instead of reporting it will most likely lead to your ^k|
^k|   ^Ndeletion. Exploitation is only allowed with the permission of an Imp.  ^k|
^k|                                                                          |
^k| ^Y* ^NBotting is allowed. Bot camping is not. Type help botting.          ^k   |                                                               |
^k|                                                                          |
^W+^k--------------------------------------------------------------------------^W+
^k|                        ^PU^ppdated: ^CNovember 23 2015                        ^k |
^W+^k--------------------------------------------------------------------------^W+^N
