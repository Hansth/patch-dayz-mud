package com.planet_ink.coffee_mud.Commands;
import com.planet_ink.coffee_mud.core.interfaces.*;
import com.planet_ink.coffee_mud.core.*;
import com.planet_ink.coffee_mud.core.collections.*;
import com.planet_ink.coffee_mud.Abilities.interfaces.*;
import com.planet_ink.coffee_mud.Areas.interfaces.*;
import com.planet_ink.coffee_mud.Behaviors.interfaces.*;
import com.planet_ink.coffee_mud.CharClasses.interfaces.*;
import com.planet_ink.coffee_mud.Commands.interfaces.*;
import com.planet_ink.coffee_mud.Common.interfaces.*;
import com.planet_ink.coffee_mud.Exits.interfaces.*;
import com.planet_ink.coffee_mud.Items.interfaces.*;
import com.planet_ink.coffee_mud.Libraries.interfaces.*;
import com.planet_ink.coffee_mud.Locales.interfaces.*;
import com.planet_ink.coffee_mud.MOBS.interfaces.*;
import com.planet_ink.coffee_mud.Races.interfaces.*;

import java.util.*;

/*
   Copyright 2004-2015 Bo Zimmerman

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

// Modified Score by Darren Steinke AKA Hansth for Patch Dayz Mud 2015

@SuppressWarnings({"unchecked","rawtypes"})
public class Score extends Affect
{
	public Score(){}

	private final String[] access=I(new String[]{"SCORE","SC"});
	@Override public String[] getAccessWords(){return access;}

	public StringBuilder getScore(MOB mob){return getScore(mob,"");}
	public StringBuilder getScore(MOB mob, String parm)
	{
		final StringBuilder msg=new StringBuilder("^N");
		final int classLevel=mob.charStats().getClassLevel(mob.charStats().getCurrentClass());

		// Column sizes
                final int COL_LEN1=CMLib.lister().fixColWidth(15.0,mob);
                final int COL_LEN2=CMLib.lister().fixColWidth(30.0,mob);
                final int COL_LEN3=CMLib.lister().fixColWidth(13.0,mob);
                final int COL_LEN4=CMLib.lister().fixColWidth(10.0,mob);
                final int COL_LEN5=CMLib.lister().fixColWidth(7.0,mob);
		final int COL_LEN6=CMLib.lister().fixColWidth(18.0,mob);
		final int COL_LEN7=CMLib.lister().fixColWidth(22.0,mob);
		final int COL_LEN8=CMLib.lister().fixColWidth(27.0,mob);
		final int COL_LEN9=CMLib.lister().fixColWidth(35.0,mob);

			// Top spacer
			msg.append(L("^W+^k===========================================================================^W+\n\r"));

		//  If classes aren't disable and levels aren't disabled, show name and title
		if((!CMSecurity.isDisabled(CMSecurity.DisFlag.CLASSES))
		&&(!mob.charStats().getMyRace().classless())
		&&(!CMSecurity.isDisabled(CMSecurity.DisFlag.LEVELS))
		&&(!mob.charStats().getMyRace().leveless())
		&&(!mob.charStats().getCurrentClass().leveless()))
		{
			String levelStr=null;
			if(classLevel>=mob.phyStats().level())
				levelStr=L("level ")+mob.phyStats().level()+" "+mob.charStats().getCurrentClass().name(mob.charStats().getCurrentClassLevel());
			else
				levelStr=mob.charStats().getCurrentClass().name(mob.charStats().getCurrentClassLevel())+" "+classLevel+"/"+mob.phyStats().level();
			final StringBuilder showName=new StringBuilder("");
			final StringBuilder showTitle=new StringBuilder("");
			showName.append("^GName: ^g"+mob.Name());
			showTitle.append("^GTitle: ^g"+mob.titledName());
			msg.append(L("^k| "+CMStrings.padRight(showName.toString(),COL_LEN2)));
			msg.append(L(""+CMStrings.padRight(showTitle.toString(),COL_LEN2)+"            ^k  |\n\r"));
		}
		else // If levels aren't disabled but classes are, show name and title
		if((!CMSecurity.isDisabled(CMSecurity.DisFlag.LEVELS))
		&&(!mob.charStats().getCurrentClass().leveless())
		&&(!mob.charStats().getMyRace().leveless()))
		{
			String levelStr=null;
			if(classLevel>=mob.phyStats().level())
				levelStr=L(", level ")+mob.phyStats().level();
			else
				levelStr=L(", level ")+classLevel+"/"+mob.phyStats().level();
			msg.append(L("You are ^H@x1^?^H@x2^?.\n\r",mob.Name(),levelStr));
		}
		else // If classes are disabled, but levels are, show name and title
		if((!CMSecurity.isDisabled(CMSecurity.DisFlag.CLASSES))
		&&(!mob.charStats().getMyRace().classless()))
		{
			final StringBuilder showName=new StringBuilder("");
			final StringBuilder showTitle=new StringBuilder("");
			showName.append("^GName: ^g"+mob.Name());
			showTitle.append("^GTitle: ^g"+mob.titledName());
			msg.append(L("^k| "+CMStrings.padRight(showName.toString(),COL_LEN2)));
			msg.append(L(""+CMStrings.padRight(showTitle.toString(),COL_LEN2)+"      ^k        |\n\r"));
		}
		else // If levels are disabled but classes aren't, show name and title
			msg.append(L("You are ^H@x1^?.\n\r",mob.Name()));

			// Spacer
			msg.append(L("^W+^k===========================================================================^W+\n\r"));

		// Class and subclass listing if classes are enabled
		if((!CMSecurity.isDisabled(CMSecurity.DisFlag.CLASSES))
		&&(classLevel<mob.phyStats().level()))
		{
			final StringBuilder classList=new StringBuilder("");
			for(int c=0;c<mob.charStats().numClasses()-1;c++)
			{
				final CharClass C=mob.charStats().getMyClass(c);
				if(C!=mob.charStats().getCurrentClass())
				{
					if(classList.length()>0)
						if(c==mob.charStats().numClasses()-2)
							classList.append(L(", and "));
						else
							classList.append(", ");
					classList.append(C.name(mob.charStats().getClassLevel(C))+" ("+mob.charStats().getClassLevel(C)+") ");
				}
			}
			final StringBuilder levelStr2=new StringBuilder("");
				levelStr2.append(mob.charStats().getCurrentClass().name(mob.charStats().getCurrentClassLevel())+" ^P(^p"+mob.charStats().getCurrentClassLevel()+"^P)");
			final StringBuilder showClass=new StringBuilder("");
			final StringBuilder showSubClass=new StringBuilder("");
			showClass.append("^PClass: ^p"+levelStr2.toString()+"");
			showSubClass.append("^PSub Class: ^p"+classList.toString()+"");
			msg.append(L("^k| "+CMStrings.padRight(showClass.toString(),COL_LEN2)));
			msg.append(L(""+CMStrings.padRight(showSubClass.toString(),COL_LEN2)+"             ^k |\n\r"));

			// Spacer
			msg.append(L("^W+^k===========================================================================^W+\n\r"));
		}
		else // Class and subclass listing is classes are disabled
		{
			final StringBuilder classList=new StringBuilder("");
			for(int c=0;c<mob.charStats().numClasses()-1;c++)
			{
				final CharClass C=mob.charStats().getMyClass(c);
				if(C!=mob.charStats().getCurrentClass())
				{
					if(classList.length()>0)
						if(c==mob.charStats().numClasses()-2)
							classList.append(L(", and "));
						else
							classList.append(", ");
					classList.append(C.name(mob.charStats().getClassLevel(C))+" ("+mob.charStats().getClassLevel(C)+") ");
				}
			}
			final StringBuilder levelStr2=new StringBuilder("");
				levelStr2.append(mob.charStats().getCurrentClass().name(mob.charStats().getCurrentClassLevel())+" ^P(^p"+mob.charStats().getCurrentClassLevel()+"^P)");
			final StringBuilder showClass=new StringBuilder("");
			final StringBuilder showSubClass=new StringBuilder("");
			showClass.append("^PClass: ^p"+levelStr2.toString()+"");
			showSubClass.append("^PSub Class: ^p"+classList.toString()+"");
			msg.append(L("^k| "+CMStrings.padRight(showClass.toString(),COL_LEN2)));
			msg.append(L(""+CMStrings.padRight(showSubClass.toString(),COL_LEN2)+"             ^k |\n\r"));

			// Spacer
			msg.append(L("^W+^k===========================================================================^W+\n\r"));
		}

		// Age and sex of character
		String genderName=L("Neuter");
		if(mob.charStats().getStat(CharStats.STAT_GENDER)=='M') 
			genderName=L("Male");
		else
		if(mob.charStats().getStat(CharStats.STAT_GENDER)=='F') 
			genderName=L("Female");
			final StringBuilder showRace=new StringBuilder("");
			showRace.append("^BRace: ^b"+mob.charStats().getMyRace().name()+"");
			msg.append(L("^k| "+CMStrings.padRight(showRace.toString(),COL_LEN2)+""));
				final StringBuilder listAge=new StringBuilder("");
				listAge.append("^BAge: ^b"+mob.baseCharStats().getStat(CharStats.STAT_AGE)+"");
				final StringBuilder showSex=new StringBuilder("");
				showSex.append("^BSex: ^b"+genderName+"");
			msg.append(L(""+CMStrings.padRight(listAge.toString(),COL_LEN7)));
			msg.append(L(""+CMStrings.padRight(showSex.toString(),COL_LEN7)+"^k|\n\r"));

			// If character is immortal/staff, show department
			if (CMSecurity.isAllowedAnywhere(mob,CMSecurity.SecFlag.WIZINV))
				{
					final StringBuilder showDept=new StringBuilder("");
					showDept.append("^BDepartment: ^b"+mob.playerStats().getDepartment()+"");
					msg.append(L("^k| "+CMStrings.padRight(showDept.toString(),COL_LEN2)+"                                            ^k|\n\r"));
				}

			// Spacer
			msg.append(L("^W+^k===========================================================================^W+\n\r"));

			// Character stats (str, int, dex, con, cha, wis)
			final StringBuilder showStr=new StringBuilder("");
			final StringBuilder showInt=new StringBuilder("");	
			final StringBuilder showDex=new StringBuilder("");
			final StringBuilder showCon=new StringBuilder("");
			final StringBuilder showCha=new StringBuilder("");
			final StringBuilder showWis=new StringBuilder("");
			showStr.append("^RStrength:     ^r"+mob.charStats().getStat(CharStats.STAT_STRENGTH)+"^w/^r"+mob.charStats().getMaxStat(CharStats.STAT_STRENGTH));
			showInt.append("^RIntelligence: ^r"+mob.charStats().getStat(CharStats.STAT_INTELLIGENCE)+"^w/^r"+mob.charStats().getMaxStat(CharStats.STAT_INTELLIGENCE));
			showDex.append("^RDexterity: ^r"+mob.charStats().getStat(CharStats.STAT_DEXTERITY)+"^w/^r"+mob.charStats().getMaxStat(CharStats.STAT_DEXTERITY));
			showCon.append("^RConstitution: ^r"+mob.charStats().getStat(CharStats.STAT_CONSTITUTION)+"^w/^r"+mob.charStats().getMaxStat(CharStats.STAT_CONSTITUTION));
			showCha.append("^RCharisma:     ^r"+mob.charStats().getStat(CharStats.STAT_CHARISMA)+"^w/^r"+mob.charStats().getMaxStat(CharStats.STAT_CHARISMA));
			showWis.append("^RWisdom:    ^r"+mob.charStats().getStat(CharStats.STAT_WISDOM)+"^w/^r"+mob.charStats().getMaxStat(CharStats.STAT_WISDOM));
                        msg.append(L("^k| "+CMStrings.padRight(showStr.toString(),COL_LEN2)+""));
                        msg.append(L(""+CMStrings.padRight(showInt.toString(),COL_LEN7)+""));
                        msg.append(L(""+CMStrings.padRight(showDex.toString(),COL_LEN7)+"^k|\n\r"));
                        msg.append(L("^k| "+CMStrings.padRight(showCon.toString(),COL_LEN2)));
                        msg.append(L(""+CMStrings.padRight(showCha.toString(),COL_LEN7)));
                        msg.append(L(""+CMStrings.padRight(showWis.toString(),COL_LEN7)+"^k|\n\r"));

			// Spacer
			msg.append(L("^W+^k===========================================================================^W+\n\r"));

			// Character current/max hitpoints
			final StringBuilder showHp=new StringBuilder("");
			showHp.append("^CHitpoints: ^c"+mob.curState().getHitPoints()+"^w/^c"+mob.maxState().getHitPoints());
			msg.append(L("^k| "+CMStrings.padRight(showHp.toString(),COL_LEN2)+""));

			// Character current/max mana
			final StringBuilder showMana=new StringBuilder("");
			showMana.append("^CMana: ^c"+mob.curState().getMana()+"^w/^c"+mob.maxState().getMana());
			msg.append(L(""+CMStrings.padRight(showMana.toString(),COL_LEN7)+""));

			// Character current/max moves
			final StringBuilder showMovement=new StringBuilder("");
			showMovement.append("^CMovement: ^c"+mob.curState().getMovement()+"^w/^c"+mob.maxState().getMovement());
			msg.append(L(""+CMStrings.padRight(showMovement.toString(),COL_LEN7)+"^k|\n\r"));

			// Spacer
			msg.append(L("^W+^k===========================================================================^W+\n\r"));

			// Character Attack
			final StringBuilder showAttack=new StringBuilder("");
			showAttack.append("^YAttack: ^y"+CMLib.combat().fightingProwessStr(mob));
			msg.append(L("^k| "+CMStrings.padRight(showAttack.toString(),COL_LEN9)+""));

			// Character defense
			final StringBuilder showDefense=new StringBuilder("");
			showDefense.append("^YDefense: ^y"+CMLib.combat().armorStr(mob));
			msg.append(L(""+CMStrings.padRight(showDefense.toString(),COL_LEN9)+"    ^k|\n\r"));

			// Spacer
			msg.append(L("^W+^k===========================================================================^W+\n\r"));

			// Character height
			final StringBuilder showHeight=new StringBuilder("");
			showHeight.append("^GHeight:        ^g"+mob.phyStats().height()+" ^Ginches");
                        msg.append(L("^k| "+CMStrings.padRight(showHeight.toString(),COL_LEN2)+""));

			// Character weight
			final StringBuilder showWeight=new StringBuilder("");
			showWeight.append("^GWeight:      ^g"+mob.baseWeight()+" ^Gpounds");
                        msg.append(L(""+CMStrings.padRight(showWeight.toString(),COL_LEN2)+"              ^k|\n\r"));

			// Number of items carried in inventory & equipped
			final StringBuilder showItems=new StringBuilder("");
			showItems.append("^GItems Carried: ^g"+mob.numItems()+"^G/^g"+mob.maxItems());
			msg.append(L("^k| "+CMStrings.padRight(showItems.toString(),COL_LEN2)+""));

			// Weight of items in inventory & equipped
			final StringBuilder showItemWeight=new StringBuilder("");
			showItemWeight.append("^GItem Weight: ^g"+mob.phyStats().weight()+" ^Gpounds");
			msg.append(L(""+CMStrings.padRight(showItemWeight.toString(),COL_LEN2)+"              ^k|\n\r"));

			// Spacer
                        msg.append(L("^W+^k===========================================================================^W+\n\r"));

			// Practice points
			final StringBuilder showPractice=new StringBuilder("");
			showPractice.append("^PPractices:      ^p"+mob.getPractices());
			msg.append(L("^k| "+CMStrings.padRight(showPractice.toString(),COL_LEN2)+""));

			// Training points
			final StringBuilder showTrain=new StringBuilder("");
			showTrain.append("^PTrains: ^p"+mob.getTrains());
			msg.append(L(""+CMStrings.padRight(showTrain.toString(),COL_LEN7)+""));

			// Quest points
			final StringBuilder showQuestPoint=new StringBuilder("");
			showQuestPoint.append("^PQuest Points: ^p"+mob.getQuestPoint());
			msg.append(L(""+CMStrings.padRight(showQuestPoint.toString(),COL_LEN7)+"^k|\n\r"));

			// Experience and exp to level
			final StringBuilder showExp=new StringBuilder("");
			final StringBuilder showExpDeLevel=new StringBuilder("");
			final StringBuilder showExpToLevel=new StringBuilder("");
			showExp.append("^PExperience Pts: ^p"+mob.getExperience()+"^P");
			showExpDeLevel.append("^POver Level: ^p"+mob.getExpNeededDelevel());
			showExpToLevel.append("^PExp to Level: ^p"+mob.getExpNeededLevel());
			msg.append(L("^k| "+CMStrings.padRight(showExp.toString(),COL_LEN2)+""));
                                 if(((CMProps.getIntVar(CMProps.Int.LASTPLAYERLEVEL)>0)
                                        &&(mob.basePhyStats().level()>CMProps.getIntVar(CMProps.Int.LASTPLAYERLEVEL)))
                                ||(mob.getExpNeededLevel()==Integer.MAX_VALUE)
                                ||(mob.charStats().isLevelCapped(mob.charStats().getCurrentClass())))
					msg.append(L(""+CMStrings.padRight(showExpDeLevel.toString(),COL_LEN7)+""));
				else
					msg.append(L(""+CMStrings.padRight(showExpToLevel.toString(),COL_LEN7)+""));

				// PK flag
				if (mob.isAttributeSet(MOB.Attrib.PLAYERKILL)) {
					final StringBuilder showPKON=new StringBuilder("");
					showPKON.append("^PPK Flag: ^pON");
					msg.append(L(""+CMStrings.padRight(showPKON.toString(),COL_LEN7)+"^k|\n\r"));}
				else {
					final StringBuilder showPKOFF=new StringBuilder("");
					showPKOFF.append("^PPK Flag: ^pOFF");
					msg.append(L(""+CMStrings.padRight(showPKOFF.toString(),COL_LEN7)+"^k|\n\r"));}

			// Spacer
                        msg.append(L("^W+^k===========================================================================^W+\n\r"));


			// Allegiance status
                        final String allegID="allegiance.ini";
                        final Faction F=CMLib.factions().getFaction(allegID);
                        if(F!=null)
                        {
				final StringBuilder showAlleg=new StringBuilder("");
                                final int allegAmt=mob.fetchFaction(allegID);
                                final Faction.FRange FR=CMLib.factions().getRange(allegID,allegAmt);
				showAlleg.append("^BAllegiance: ^b"+FR.name());
                                if((FR!=null)&&(F.showInScore()))
			msg.append(L("^k| "+CMStrings.padRight(showAlleg.toString(),COL_LEN2)+""));
                        }

			// Alignment status
                        final String align2ID="alignment.ini";
                        final Faction F2=CMLib.factions().getFaction(align2ID);
                        if(F2!=null)
                        {
				final StringBuilder showAlign2=new StringBuilder("");
                                final int align2Amt=mob.fetchFaction(align2ID);
                                final Faction.FRange FR2=CMLib.factions().getRange(align2ID,align2Amt);
				showAlign2.append("^BAlignment: ^b"+FR2.name()+" ^B(^b"+align2Amt+"^B)");
                                if((FR2!=null)&&(F2.showInScore()))
			msg.append(L(""+CMStrings.padRight(showAlign2.toString(),COL_LEN2)+"              ^k|\n\r"));
                        }

		// Clan and clan rank
                if(mob.clans().iterator().hasNext())
                {
                        for(final Iterator<Pair<Clan,Integer>> c = mob.clans().iterator();c.hasNext();)
                        {
                                final Pair<Clan,Integer> p=c.next();
                                final Clan C=p.first;
                                String role=C.getRoleName(p.second.intValue(),true,false);
                   //             role=CMLib.english().startWithAorAn(role);
			final StringBuilder showClan=new StringBuilder("");
			final StringBuilder showRank=new StringBuilder("");
			showClan.append("^BClan: ^b"+C.getName());
			showRank.append("^BRank: ^b"+role);
			msg.append(L("^k| "+CMStrings.padRight(showClan.toString(),COL_LEN2)+""));
			msg.append(L(""+CMStrings.padRight(showRank.toString(),COL_LEN2)+"              ^k|\n\r"));
                        }

                }
		else // If they don't have a clan or clan rank
		{
			final StringBuilder showClanNone=new StringBuilder("");
			final StringBuilder showRankNone=new StringBuilder("");
			showClanNone.append("^BClan: ^bNone");
			showRankNone.append("^BRank: ^bNone");
			msg.append(L("^k| "+CMStrings.padRight(showClanNone.toString(),COL_LEN2)+""));
			msg.append(L(""+CMStrings.padRight(showRankNone.toString(),COL_LEN2)+"              ^k|\n\r"));
		}

			// Spacer
                        msg.append(L("^W+^k===========================================================================^W+\n\r"));

			// Toon that character is married to
			if (mob.getLiegeID().length()>0){
				final StringBuilder showMarried=new StringBuilder("");
				showMarried.append("^YMarried to: ^y"+mob.getLiegeID());
				msg.append(L("^k| "+CMStrings.padRight(showMarried.toString(),COL_LEN2)+""));}
			else { // Not married
				final StringBuilder showNotMarried=new StringBuilder("");
				showNotMarried.append("^YMarried to: ^yNo One");
				msg.append(L("^k| "+CMStrings.padRight(showNotMarried.toString(),COL_LEN2)+""));}

			// Deity that character worships
			if(mob.getWorshipCharID().length()>0){
				final StringBuilder showWorship=new StringBuilder("");
				showWorship.append("^YWorships: ^y"+mob.getWorshipCharID());
				msg.append(L(""+CMStrings.padRight(showWorship.toString(),COL_LEN2)+"              ^k|\n\r"));}
			else { // No deity
				final StringBuilder showNoWorship=new StringBuilder("");
				showNoWorship.append("^YWorships: ^yNo One");
				msg.append(L(""+CMStrings.padRight(showNoWorship.toString(),COL_LEN2)+"              ^k|\n\r"));}

			// Spacer
                        msg.append(L("^W+^k===========================================================================^W+\n\r"));

			// Position of character
			final StringBuilder showPos=new StringBuilder("");
			final StringBuilder thePos=new StringBuilder("");
                 if(CMLib.flags().isBound(mob))
                        thePos.append("Bound.");

                if(CMLib.flags().isSleeping(mob))
                        thePos.append("Sleeping^?");
                else
                if(CMLib.flags().isSitting(mob))
                        thePos.append("Resting^?");
                else
                if(CMLib.flags().isSwimmingInWater(mob))
                        thePos.append("Swimming^?");
                else
                if(CMLib.flags().isClimbing(mob))
                        thePos.append("Climbing^?");
                else
                if(CMLib.flags().isFlying(mob))
                        thePos.append("Flying^?");
                else
                        thePos.append("Standing^?");
			showPos.append("^RPosition: ^r"+thePos.toString());
			msg.append(L("^k| "+CMStrings.padRight(showPos.toString(),COL_LEN2)+""));

			// Riding status
			final StringBuilder showRiding=new StringBuilder("");
			if (mob.riding()!=null) 
				showRiding.append("^RRiding: ^r"+mob.riding().name());
			else
				showRiding.append("^RRiding: ^rNothing");
			msg.append(L(""+CMStrings.padRight(showRiding.toString(),COL_LEN2)+"^k              |\n\r"));

			// Wimpy set to x hitpoints
			final StringBuilder showWimpy=new StringBuilder("");
			showWimpy.append("^RWimpy: ^r"+mob.getWimpHitPoint()+" ^RHP");
			msg.append(L("^k| "+CMStrings.padRight(showWimpy.toString(),COL_LEN2)+""));

			// Online for x hours
			final StringBuilder showOnlineFor=new StringBuilder("");
			showOnlineFor.append("^ROnline for: ^r"+Math.round(CMath.div(mob.getAgeMinutes(),60.0))+" ^Rhours");
			msg.append(L(""+CMStrings.padRight(showOnlineFor.toString(),COL_LEN2)+"   ^k           |\n\r"));

			// Spacer
                        msg.append(L("^W+^k===========================================================================^W+\n\r"));

		// Send it all!
		return msg;
	}

	@Override
	public boolean execute(MOB mob, List<String> commands, int metaFlags)
		throws java.io.IOException
	{
		String parm="";
		if(commands.size()>1)
			parm=CMParms.combine(commands,1);
		final StringBuilder msg=getScore(mob,parm);
		if(commands.size()==0)
		{
			commands.add(msg.toString());
			return false;
		}
		if(!mob.isMonster())
			mob.session().wraplessPrintln(msg.toString());
		return false;
	}

	@Override public boolean canBeOrdered(){return true;}


}
