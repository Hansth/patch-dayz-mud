^W+^k--------------------------------------------------------------------------^W+
^k|                            ^GM^gessage of the Da^Gy                          ^k  |
^k|                        ^gP^Gatch Dayz Development Por^gt                     ^k  |
^W+^k--------------------------------------------------------------------------^W+
^k|                                                                          |
^k| ^Y* ^NWelcome to the development port. This port is for beta testing and ^k    |
^k|   ^Nand development. The main production port is on port ^R6069^N.^k             |
^k|                                                                          |
^k| ^Y* ^NIf you see a bug, report it. Use the ^RBUG ^Ncommand. Do not exploit it.   ^k|
^k|                                                                          |
^k| ^Y* ^NThis port can be unstable at times. We frequently pull SVN updates  ^k   |
^k|   ^Nfrom CoffeeMud, and they are alpha and beta features. This port can    ^k|
^k|   ^Ncrash, delete things, look wierd etc. Be warned.                       ^k|
^k|                                                                          |
^k| ^Y* ^NOur development port and production port use 2 seperate databases. So ^k |
^k|   ^Ncharacters on this port may or may not be represented on the other     ^k|
^k|   ^Nport.                                                                  ^k|
^k|                                                                          |   
^k| ^Y* ^NIf you have any questions, or want to help, ask any Implementor or   ^k  |
^k|   ^Ntalk to ^RHansth^N. Also check out the website ^Rpatchdayz.net            ^k |
^k|                                                                          |
^W+^k--------------------------------------------------------------------------^W+
^k|                        ^GU^gpdated: ^cNovember 23 2015                       ^k  |
^W+^k--------------------------------------------------------------------------^W+^N
